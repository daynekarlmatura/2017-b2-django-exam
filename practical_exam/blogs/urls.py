from django.conf.urls import url

from .views import blog_index, blog_post_detail, blog_section_list, contact_us

urlpatterns = [
    url(r'^$', blog_index, name="home"),
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$',
        blog_post_detail,
        name="blog_post"),
    url(r'^post/(?P<post_pk>\d+)/$', blog_post_detail, name="blog_post_pk"),
#    url(r'^(?P<section_slug>[-\w]+)/$', blog_section_list, name="blog_section"),
    url(r'^contact_us/$', contact_us, name="contact-us"),
]
