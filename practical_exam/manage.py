#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "practical_exam.settings")

    from django.core.management import execute_from_command_line
    import practical_exam.startup as startup
    import django
    django.setup()
    startup.run()
    execute_from_command_line(sys.argv)
