from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static

from django.contrib import admin
from django.contrib.auth import views as auth_views


from django.contrib import admin

urlpatterns = [
	url(r'^login/$', auth_views.login, {'template_name': 'blogs/blog_login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'template_name': 'blogs/blog_logout.html'}, name='logout'),
    url(r"^admin/", include(admin.site.urls)),
    url(r"", include("blogs.urls")),
]


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
